//
//  Comment.swift
//  EsokiaSocialApp
//
//  Created by Avinash Lingaloo on 6/27/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import Foundation

class Comment{
    
    // MARK: Properties
   
    var comment:String?
    var timeC:Int?
    var username:String?
    var UUID:String?
    
    // MARK: Constructor
    
    init(comment:String, time:Int,uuid:String, userName:String ){
        self.comment = comment
        self.timeC = time
        self.username=userName
        self.UUID = uuid
    }

    
//    func toAnyObject(commentObject: Comment) -> AnyObject {
//        return [  "comment":commentObject.comment,
//                  "time":commentObject.timeC,
//                  "uid":commentObject.UUID,
//                  "username": commentObject.username]
//        
//    }
    
    
    
//    var comment:String{
//        get{
//            return self.comment
//        }
//        set(newComment){
//            self.comment = newComment
//        }
//    }
//    
//    var UUID:String {
//        get {
//            return self.UUID
//        }
//        
//        set(newUUID) {
//            self.UUID = newUUID
//        }
//    }
//    
//    var commentTime:Int {
//        get {
//            return self.commentTime
//        }
//        
//        set(newTime) {
//            self.commentTime = newTime
//        }
//    }
//    
//    var username:String {
//        get {
//            return self.username
//        }
//        
//        set(newUSN) {
//            self.username = newUSN
//        }
//    }
    

}































