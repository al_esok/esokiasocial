//
//  PostManager.swift
//  EsokiaSocial
//
//  Created by Avinash Lingaloo on 6/24/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import Foundation
import Firebase
import Photos
import FirebaseStorage


typealias CompletionHandler = (success:Bool) -> Void
typealias CompletionHandlerLike = (success:Bool, likes:String) -> Void

class PostManager{
    
    //MARK: Properties
    static let postRef = Statics.firebaseRef().child(POSTREF)
    static let UUID = UserManager.retrieveUUID()
    static let userName = UserManager.retrieveUsername()
    static let milliseconds = Int( NSDate().timeIntervalSince1970 * 1000.0 ) // time in milliseconds
    static  var storageRef:FIRStorageReference?
    
    static func postNewComment(comment:String, key:String, count:Int)->Void{
        
        // let newComment = Comment.init(comment: comment, time: milliseconds, uuid: UUID, userName: userName)
        
        let newComment = ["comment": comment,
                          "time": milliseconds,
                          "uid": UUID,
                          "username": userName]
        
        let childUpdates = ["\(count)": newComment ]
        let currentRef = postRef.child(key).child("comments")//.childByAutoId()
        
        // Write data to Firebase
        currentRef.updateChildValues(childUpdates)
        
    }
    
    static func postNewPost(comment:String, referenceURL:NSURL, withCompletionHandler:CompletionHandler){
        storageRef = Statics.firebaseStorageRef()
        let assets = PHAsset.fetchAssetsWithALAssetURLs([referenceURL], options: nil)
        let asset = assets.firstObject
        
        asset?.requestContentEditingInputWithOptions(nil, completionHandler: { (contentEditingInput,info) in
            let imageFile = contentEditingInput?.fullSizeImageURL
            let filePath = FIRAuth.auth()!.currentUser!.uid +
                "/\(Int(NSDate.timeIntervalSinceReferenceDate() * 1000))/\(imageFile!.lastPathComponent!)"
            
            // [START uploadimage]
            self.storageRef!.child(filePath)
                .putFile(imageFile!, metadata: nil) { (metadata, error) in
                    if let error = error {
                        print("Error uploading: \(error)")
                        //self.urlTextView.text = "Upload Failed"
                        withCompletionHandler(success: true)
                        return
                    }
                    else{
                        let newComment = [[  "comment": comment,
                            "time": milliseconds,
                            "uid": UUID,
                            "username": userName
                        ]]
                        let uploadedImageURL = metadata?.downloadURL()?.absoluteString
                        let likes = "0"
                        let time = milliseconds
                        let uid = FIRAuth.auth()?.currentUser?.uid
                        let username = UserManager.retrieveUsername()//FIRAuth.auth()?.currentUser!.displayName
                        let users_liked = ["":""]
                        
                        var newPost = [String:AnyObject]()
                        
                        newPost = [ "comments" : newComment,
                                    "image_url": uploadedImageURL!,
                                    "likes" : likes,
                                    "time":time,
                                    "uid":uid!,
                                    "username": username,
                                   // "users_liked":users_liked
                        ]
             
                        let currentRef = postRef.childByAutoId()
                        
                        // Write data to Firebase
                        currentRef.setValue(newPost)
                        
                        withCompletionHandler(success: true)

                        
                    }
                    
            }
            // [END uploadimage]
        })
        
        
        
        
    }
    
    static func adjustLike(postC:Post, likeValue:Int , withCompletionHandler:CompletionHandlerLike) -> String {
        
       // var toRemove = likeValue
        var alreadyLikedPost = false
        //retrieve like count
        var likesCount = Int(postC.user_likes!)
        
        if(likeValue == 1 ){
            likesCount = likesCount! + 1
        }
        else{
            if(likesCount == 0){
                likesCount = 0
            }
            else{
                likesCount = likesCount! - 1
            }
            
        }
        // convert like count to string
        var likes = ""
        if let lkc = likesCount{
            likes = "\(lkc)"
        }
        
        let key = postC.key!
        
        let currentRef = postRef.child(key).child("users_liked")//.childByAutoId()
        let likesRef = postRef.child(key).child("likes")
        
        //find out if the user already liked the post
        if let userLikesDict = postC.users_liked{
            for (keyLk,_) in userLikesDict{
                if(keyLk == UUID){
                    alreadyLikedPost = true
                    break
                }
            }
        }
        if(alreadyLikedPost){
            //print("UUID is \(UUID)")
            if(likeValue == 0 ){
                var newUsers_liked:[String:AnyObject] = postC.users_liked!
                newUsers_liked.removeValueForKey(UUID)
                currentRef.setValue(newUsers_liked)
            }
        }
        else{
            let newlike = [UUID: UUID]
            if var currentDict = postC.users_liked{
                 currentDict[UUID] = UUID
                currentRef.setValue(currentDict)
            }
            else{
                currentRef.updateChildValues(newlike)
            }
           
            
        }
        
        likesRef.setValue(likes)
        withCompletionHandler(success: true, likes:likes)
        return likes
    
    }
    
}




























