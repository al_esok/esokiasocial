//
//  CellView.swift
//  EsokiaSocialApp
//
//  Created by Avinash Lingaloo on 6/27/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import Foundation
import UIKit

class CellView: UITableViewCell{

    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var commentsLabel: UILabel!
    @IBOutlet weak var countLikes: UILabel!
    
    @IBOutlet weak var counterLikes: UILabel!
    var btnLikeToggleState = 0
    var unLikedImage = UIImage.init(named: "heart")
    var likedImage = UIImage.init(named: "likedHeart")
    var postForCell:Post?
    var strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
    var activityFrame = UIView()
    
    
      @IBOutlet weak var btnLike: UIButton!
    
    
    //there is already an imageView inside tableCell by default...hence the name customimageView to prevent name conflicts
    @IBOutlet weak var customImageView: UIImageView!
    
    @IBAction func likesButton(sender: AnyObject) {
       let nc = NSNotificationCenter.defaultCenter()
        
        if btnLikeToggleState == 0 {
            btnLikeToggleState = 1
            sender.setImage(likedImage,forState:UIControlState.Normal)
        
        }
        else {
            btnLikeToggleState = 0
            sender.setImage(unLikedImage,forState:UIControlState.Normal)
        }
        
        if let postC = postForCell{
            
           //dispatch_async(dispatch_get_main_queue()) {
                _ = PostManager.adjustLike(postC, likeValue: self.btnLikeToggleState, withCompletionHandler: { (success,likes) in
                    if(success){
                    
                        self.counterLikes.text = likes
                        print("new like is \(likes)")
                        let myDict = [ "likes": likes]
                        nc.postNotificationName("reloadPostNotif", object: myDict)
                    }
                })
            }
            
           
        }

    //}
    

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

















