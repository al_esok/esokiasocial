//
//  NewPostViewController.swift
//  EsokiaSocial
//
//  Created by Avinash Lingaloo on 6/24/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//
import UIKit
import Photos
import Firebase



class NewPostViewController: UIViewController,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextViewDelegate {
    
    // MARK: Properties
    var picker:UIImagePickerController? = UIImagePickerController()
    var postCompleted = 0
    var activityFrame = UIView()
    var referenceURL:NSURL?
    var strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
    
    // MARK: Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnImagePicker: UIButton!
    
    
    //Publish le post a Firebase
    @IBAction func btnDone(sender: AnyObject) {
        print("sending new post to Firebase")
        
        let defaultText = textView.text
        let isDefaultText = defaultText.containsString("Type your thoughts here...")
        let newComment = textView.text
        
        if(!isDefaultText){
            // PostManager.postNewPost(newComment, referenceURL:referenceURL!)
            
            
            let loader = self.createLoader()
            
            PostManager.postNewPost(newComment, referenceURL: referenceURL!, withCompletionHandler: { (success) in
                if(success){
                    loader.stopAnimating()
                    self.activityFrame.removeFromSuperview()
                    self.dismissViewControllerAnimated(true){}
                }
            })
        }
        
    }
    
    deinit{
        print("new post VC deinit")
        
    }
    
    func createLoader() -> UIActivityIndicatorView {
       
        strLabel.text = "Uploading.."
        strLabel.textColor = UIColor.whiteColor()
        activityFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25 , width: 180, height: 50))
        activityFrame.layer.cornerRadius = 15
        activityFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)

          let   activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            activityIndicator.startAnimating()
            activityFrame.addSubview(activityIndicator)

        activityFrame.addSubview(strLabel)
        view.addSubview(activityFrame)
        return activityIndicator
    }
    
    
    @IBAction func btnBack(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) {}
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Imitation du placeholder du TextField au TextView
        textView.textColor = UIColor.lightGrayColor()
        textView.text = "Type your thoughts here..."
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Image Picker
    @IBAction func btnImagePickerClicked(sender: AnyObject)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openCamera()
            
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
        {
            UIAlertAction in
            self.navigationController?.popViewControllerAnimated(true)
            
        }
        
        // Add the actions
        picker?.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        // Present the controller
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func openCamera(){
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            self .presentViewController(picker!, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    func openGallary()
    {
        picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(picker!, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        picker.dismissViewControllerAnimated(true, completion: nil)
        imageView.image=info[UIImagePickerControllerOriginalImage] as? UIImage
        postCompleted = 1
        btnImagePicker.hidden = true
        referenceURL = (info[UIImagePickerControllerReferenceURL] as? NSURL?)!
        
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        print("picker cancel.")
        picker.dismissViewControllerAnimated(true, completion: nil)
        //self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK: UITextView
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        textView.text = ""
        return true
    }
    func textViewDidEndEditing (textView: UITextView) {
        if (textView.text.isEmpty || textView.text == "" ){
            textView.textColor = UIColor.lightGrayColor()
            textView.text = "Type your thoughts here..."
        }
        else{
            postCompleted = 2
            
        }
    }
    
    
    
    
}




























