//
//  CONSTANTS.swift
//  EsokiaSocial
//
//  Created by Avinash Lingaloo on 6/24/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import Foundation

let BASE_URL = "https://esokiaminiapp.firebaseio.com/"

let USERS = "users"

let POSTREF = "posts"
