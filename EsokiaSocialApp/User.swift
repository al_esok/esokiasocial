//
//  User.swift
//  EsokiaSocial
//
//  Created by Avinash Lingaloo on 6/24/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import Foundation


class User{
    
    // MARK: Properties
    
    var username:String{
        get{
            return self.username
        }
        set(newUSN){
            self.username = newUSN
        }
    }
    
    var UUID:String {
        get {
            return self.UUID
        }
        
        set(newUUID) {
            self.UUID = newUUID
        }
    }
    
    // MARK: Constructor
    init(userName:String, uuid:String){
        self.username=userName
        self.UUID = uuid
    }
}


































