//
//  SpecificPostViewController.swift
//  EsokiaSocial
//
//  Created by Avinash Lingaloo on 6/24/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import UIKit

class SpecificPostViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var spTableView: UITableView!
    @IBOutlet weak var spPostImageView: UIImageView!
    
    @IBOutlet weak var navBar: UINavigationBar!
    
    //MARK: Properties
    var spPost:Post?
    var postImage:UIImage!


 
    
    
    
    //MARK: ViewControllerLifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
            spPostImageView.image = postImage
        
       
    }
    override func viewWillAppear(animated: Bool) {
        spTableView.reloadData()
        self.title =  UserManager.retrieveUsername()
    }
    
    @IBAction func goBack(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) {}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("in specific post, length of post's comments is ", spPost?.commentArray?.count)
        if let numRows = spPost?.commentArray?.count {
            return numRows
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("spPostCell") //as! CellView
       
        if let theComment = spPost?.commentArray![indexPath.row]{
            cell!.textLabel?.text = theComment.comment
        }

        return cell!
    }
 
    deinit{
        print("Specific deallocated")
    }
    
    override func prepareForSegue(segue:UIStoryboardSegue?, sender: AnyObject!) {
  
        
        if (segue!.identifier == "newComment") {
//            let indexPath = tableview.indexPathForSelectedRow!
//            let selectedPost = post[indexPath.row]
//            let selectedCell =  tableview.cellForRowAtIndexPath(indexPath) as! CellView
//            let postImage = selectedCell.customImageView.image
            let destinationVC = segue!.destinationViewController as! NewCommentVC
            destinationVC.key = self.spPost?.key
            destinationVC.count = self.spPost?.commentArray?.count

        }
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}