//
//  Post.swift
//  EsokiaSocial
//
//  Created by Avinash Lingaloo on 6/24/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Post{
    
    // MARK: Properties
    var imagePost:String? = ""
    var user_likes:String? = ""
    var comment:[[String:AnyObject]]?// = [Comment]()
    var time:Int? = 0
    var username:String? = ""
    var UUID:String? = ""
    var key:String? = ""
    var commentArray:[Comment]?
    var users_liked:[String:String]?
    //addon
    var postDate: NSDate? = NSDate()
    
    // MARK: Constructor
    
    //Arbitrary constructor
    init(userName:String, uuid:String, time:NSDate, image:String, likes:String, comment:[Comment]){
        self.username = userName
        self.UUID = uuid
        //self.comment = comment
        self.imagePost=image
        self.user_likes = likes
        //self.time = time
        self.key = "Arbitrary key"
    }
    //Constructor when using Firebase
    init(snapshot: FIRDataSnapshot) {
       //print(snapshot)
        if let values = snapshot.value {
            self.key = snapshot.key
            self.username = values["username"] as? String
            self.UUID = values["uid"] as? String
            self.comment = values["comments"] as? [[String:AnyObject]]
            self.imagePost = values["image_url"] as? String
            self.user_likes = values["likes"] as? String
            self.time = values["time"] as? Int
            self.users_liked = values["users_liked"] as? [String:String]
            self.postDate = NSDate(timeIntervalSince1970: Double(self.time ?? NSDate().timeIntervalSince1970))
            self.commentArray = []
        }
    }
}