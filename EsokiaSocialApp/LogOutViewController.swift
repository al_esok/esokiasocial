//
//  LogOutViewController.swift
//  EsokiaSocialApp
//
//  Created by Avinash Lingaloo on 7/1/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LogOutViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBack(sender: AnyObject) {
        self.dismissViewControllerAnimated(true){}
        
    }
    @IBAction func btn_LogOut(sender: AnyObject) {
        
        do {
            try FIRAuth.auth()?.signOut()
        }
        catch _ {
            print("Error during logout process")
        }
        
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
    
    deinit{
        print("LogOut VC deinit")
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
