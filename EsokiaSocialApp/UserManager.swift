//
//  UserManager.swift
//  EsokiaSocial
//
//  Created by Avinash Lingaloo on 6/24/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import Foundation


class UserManager{
    
    //MARK : Properties
    static let userRef = Statics.firebaseRef().child(USERS)
  //  static let
    
    
    //MARK : Add user to firebase
    static func createUserFirebase(usn:String) -> Void {
    
        let newUser = ["likes":"0","username":usn]
        
        //et childUpdates = ["\(count)": newComment ]
        let currentRef = userRef.childByAutoId()
        
        // Write data to Firebase
        currentRef.setValue(newUser)
    
    
    }
    
    
    
    //MARK : Credentials
    
    static func storeCredetials(usn:String, pwd:String){
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(usn, forKey: "usernameKey")
        defaults.synchronize()
        defaults.setObject(pwd, forKey: "passwordKey")
        defaults.synchronize()
        
    }
    
    static func retrieveCredentials()->(String, String){
        var usname = ""
        var passwd = ""
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let usn = defaults.stringForKey("usernameKey") {
            usname = usn
            
        }
        if let pwd = defaults.stringForKey("passwordKey") {
            passwd = pwd
        }
        
        return (usname,passwd)
        
    }
    
    
    //MARK : UUID
    static func storeUUID(uuid:String){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(uuid, forKey: "UUID")
        defaults.synchronize()
    }
    
    static func retrieveUUID()->String{
        var uuid = ""
        let defaults = NSUserDefaults.standardUserDefaults()
        if let usn = defaults.stringForKey("UUID") {  uuid = usn }
        return uuid
    }
    
    //MARK : Username
    static func storeUsername(uuid:String){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(uuid, forKey: "username")
        defaults.synchronize()
    }
    
    static func retrieveUsername()->String{
        var username = ""
        let defaults = NSUserDefaults.standardUserDefaults()
        if let usn = defaults.stringForKey("username") {  username = usn }
        return username
    }
    
}