//
//  LoginViewController.swift
//  EsokiaSocial
//
//  Created by Avinash Lingaloo on 6/24/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    // MARK: Constants
    var  FIREBASE_ROOT:FIRDatabaseReference?
    
    // MARK: Outlets
    @IBOutlet weak var txtfld_Username: UITextField!
    @IBOutlet weak var txtfld_Password: UITextField!
    
    // MARK: Properties
    var emailUser = ""
    var pwdUser = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FIREBASE_ROOT  = Statics.firebaseRef().child(USERS)
       
        let credential =  UserManager.retrieveCredentials()
        
        let username  = credential.0
        let password = credential.1
        let emptyUsn = username.isEmpty
        let emptyPwd = password.isEmpty
        if ( !emptyPwd && !emptyUsn ){
            txtfld_Username.text = username
            txtfld_Password.text = password
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func btn_Login(sender: UIButton) {
        
        let email = txtfld_Username.text
        let password = txtfld_Password.text
        
        let userName = email?.substringToIndex(email!.characters.indexOf("@")!)
        UserManager.storeUsername(userName!)
        if(email != nil && password != nil) {
            
            FIRAuth.auth()?.signInWithEmail(email!, password: password!, completion: { (user, err) in
                if(err == nil){
                    UserManager.storeCredetials(email!,pwd: password!)
                    self.performSegueWithIdentifier("allposts", sender: nil)
                    
                }
                else{
                    print("error during login \(err!.description)")
                }
                
                UserManager.storeUUID((user?.uid)!)
            })
        }
    }
    
    deinit{
        print("Log In VC deinit")
        
    }
    
    
    
}
























