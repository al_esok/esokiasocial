//
//  RegisterViewController.swift
//  EsokiaSocial
//
//  Created by Avinash Lingaloo on 6/24/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {
    
    // MARK: Constants
    var FIREBASE_ROOT: FIRDatabaseReference?
    
    // MARK: Outlets
    
    @IBOutlet weak var txtfld_email: UITextField!
    @IBOutlet weak var txtfld_username: UITextField!
    @IBOutlet weak var txtfld_password: UITextField!
    @IBOutlet weak var txtfld_matchPwd: UITextField!
    
    // MARK: Properties

    override func viewDidLoad() {
        super.viewDidLoad()
        
       // FIREBASE_ROOT = Statics.firebaseRef().child(BASE_URL)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btn_Register(sender: AnyObject) {
        
        let email = txtfld_email.text
        let password = txtfld_password.text
       let username =  txtfld_username.text
        
        if(email != nil && password != nil && username != nil){
            FIRAuth.auth()?.createUserWithEmail(email!, password: password!, completion: { (user, err) in
                
                UserManager.createUserFirebase(username!)
                UserManager.storeCredetials(email!, pwd : password!)
                self.performSegueWithIdentifier("showCongrats", sender: nil)
            })
                   
        }//end outer if
        
        
        
        
}

    
    deinit{
        print("register view VC deinit")
        
    }
    
}//end of RegisterViewController

