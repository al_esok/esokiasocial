//
//  AllPostsViewController.swift
//  EsokiaSocial
//
//  Created by Avinash Lingaloo on 6/24/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import UIKit
import Firebase

//protocol PostSelectionDelegate: class {
//    func postSelected(post: Post)
//}

class AllPostsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    
    //MARK: Outlets
    @IBOutlet weak var tableview: UITableView!
    
    
    
    //MARK: Properties
    var post : [Post] = []
    var btnLikeToggleState = 0
    var unLikedImage = UIImage.init(named: "heart")
    var likedImage = UIImage.init(named: "likedHeart")
    var selectedImage:UIImageView?
    let UUID = UserManager.retrieveUUID()
    
    //MARK: Constants
    
    override func viewWillAppear(animated: Bool) {
        self.retrievePosts()
    }
    
  
    deinit{
        print("ALL Post VC deinit")
        
    }
    
    func retrievePosts()->Void{
        let postRef = Statics.firebaseRef().child(POSTREF)
        
        postRef.observeEventType(.Value, withBlock: { (snapshot: FIRDataSnapshot!) in
            
            // Check to see if the snapshot has any data
            if snapshot.exists() {
                self.post.removeAll()
                for child in snapshot.children{
                    let newPost = Post(snapshot: child as! FIRDataSnapshot)
                    self.post.append(newPost)
                    
                }
                self.tableview.reloadData()
                
            }
        })
         //self.tableview.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func viewDidAppear(animated: Bool) {
        let nc = NSNotificationCenter.defaultCenter()
        nc.addObserver(self, selector: #selector(reloadPosts), name: "reloadPostNotif", object: nil)

    }
    
    
    func reloadPosts() ->Void{
        print("in func reloadPosts")
        let postRef = Statics.firebaseRef().child(POSTREF)
        
        postRef.observeEventType(.ChildChanged, withBlock: { (snapshot: FIRDataSnapshot!) in
            
            // Check to see if the snapshot has any data
            if snapshot.exists() {
                self.post.removeAll()
                for child in snapshot.children{
                    
                    let newPost = Post(snapshot: child as! FIRDataSnapshot)
                    self.post.append(newPost)
                    
                }
                self.tableview.reloadData()
                
            }
        })
        self.tableview.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
 
    
    
    //MARK : TableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("length of post is ", self.post.count)
        return post.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return postCellAtIndexPath(tableView, indexPath: indexPath)
    }
    
    func postCellAtIndexPath(tableView: UITableView, indexPath:NSIndexPath) -> CellView {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("allPostCells") as! CellView
        let currentPost = post[indexPath.row]
        cell.postForCell = currentPost
        
        
        //like button
        if let usersLikedPost = currentPost.users_liked {
            for (keyLK, _) in  usersLikedPost {
                if(keyLK == UUID){
                    cell.btnLike.setImage(UIImage.init(named: "likedHeart"),forState:UIControlState.Normal)
                    cell.btnLikeToggleState = 1
                    
                }
                else{
                    cell.btnLike.setImage(UIImage.init(named: "heart"),forState:UIControlState.Normal)
                    cell.btnLikeToggleState = 0

                }
                
            }
            
        }
        var urlString = ""
        if let urlstr = currentPost.imagePost{
            
            urlString = urlstr
        }
        
        let url = NSURL(string: urlString)
        var commentArray:[Comment] =  []
        
        //loop over an array of Dictionary containing currentPost's comments
        if let theComment = currentPost.comment{
            for cmt in theComment{
                let cmmt  = cmt["comment"] as! String?
                let time = cmt["time"] as! Int?
                let uid = cmt["uid"] as! String?
                let username = cmt["username"] as! String?
                let commentObject = Comment(comment: cmmt! , time: time!, uuid: uid!, userName: username!)
                commentArray.append(commentObject)
            }
            currentPost.commentArray = commentArray
        }
        
        //if currentPost.imagePost!.lowercaseString.rangeOfString("https") != nil {}
        if let urlImage = url{ cell.customImageView.setImageWithURL(urlImage)  }
        
        cell.usernameLabel.text = currentPost.username
        cell.counterLikes.text = currentPost.user_likes
        if ( commentArray.count > 0 ){
            if let firstComment = commentArray[0].comment { cell.commentsLabel.text = String(firstComment)}
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("didselect row has been called")
    }
    
    override func prepareForSegue(segue:UIStoryboardSegue?, sender: AnyObject!) {
        print("inside of prepare for segue")
        
        var array: [String] = [String]()
        array.append("salut")
        
        if (segue!.identifier == "showPost") {
            let indexPath = tableview.indexPathForSelectedRow!
            let selectedPost = post[indexPath.row]
            let selectedCell =  tableview.cellForRowAtIndexPath(indexPath) as! CellView
            let postImage = selectedCell.customImageView.image
            let destinationVC = segue!.destinationViewController as! SpecificPostViewController
            destinationVC.postImage = postImage
            destinationVC.spPost = selectedPost
        }
    }
    
    
}










































