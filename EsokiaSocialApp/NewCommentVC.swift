//
//  NewCommentVC.swift
//  EsokiaSocialApp
//
//  Created by Avinash Lingaloo on 6/30/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import UIKit

class NewCommentVC: UIViewController, UITextViewDelegate{
    
    
    //MARK: Properties
    var key:String? = ""
    var count:Int? = 0
    
    //MARK: Outlets
    @IBOutlet weak var textViewComment: UITextView!
    
    @IBAction func btnBack(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true) {}

        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Imitation du placeholder du TextField au TextView
        textViewComment.textColor = UIColor.lightGrayColor()
        textViewComment.text = "Type your thoughts here..."
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnPostComment(sender: AnyObject)
    {
        let defaultText = textViewComment.text
        let isDefaultText = defaultText.containsString("Type your thoughts here...")
        let newComment = textViewComment.text
        
        //print("key is \(key!)")
        if(!isDefaultText){
            PostManager.postNewComment(newComment, key: key!, count:count!)
        }
        
        
    }
    deinit{
        print("newcomment VC deinit")
        
    }
    
    //MARK: UITextView
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        textView.text = ""
        return true
    }
    func textViewDidEndEditing (textView: UITextView) {
        if (textViewComment.text.isEmpty || textViewComment.text == "" ){
            textViewComment.textColor = UIColor.lightGrayColor()
            textViewComment.text = "Type your thoughts here..."
        }
        else{
            // postCompleted = 2
            
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
