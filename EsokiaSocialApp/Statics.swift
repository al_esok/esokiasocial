//
//  Statics.swift
//  EsokiaSocialApp
//
//  Created by Avinash Lingaloo on 6/29/16.
//  Copyright © 2016 Avinash Lingaloo. All rights reserved.
//

import UIKit
import Firebase

class Statics: NSObject {

    class func firebaseRef() -> FIRDatabaseReference {
        return FIRDatabase.database().reference()
    }
    class func firebaseStorageRef() -> FIRStorageReference {
        return FIRStorage.storage().referenceForURL("gs://esokiaminiapp.appspot.com")
    }
}
